# OpenVpn client for Ubuntu 16

### Installs OpenVpn client Ubuntu xenial linux.

### Dependencies

None.

### Config



#### openvpnClient_compression  
Desc:  Enable a compression algorithm (lzo)  
Ways: yes/no  
Default: ```openvpnClient_compression: yes```  
************


#### openvpnClient_device  
Desc:  Device user for OVPN connection  
Ways: tun/tap   
Default: ```openvpnClient_device: tun```  
************


#### openvpnClient_dir 
Desc:  A part of  ```openvpnClient_client_config``` contains name of dir with client`s config file     
Default: ```openvpnClient_dir: /etc/openvpn```  
************

#### openvpnClient_file  
Desc:  A part of  ```openvpnClient_client_config``` contains name of client`s config file  
Default: ```openvpnClient_file: client.conf```  
************


#### openvpnClient_port 
Desc:  Port number for remote connection   
Ways: [1..65 535]     
Default: ``` openvpnClient_port: 1194 ```  
************


#### openvpnClient_protocol 
Desc:  Protocol used for connection   
Ways: tcp/udp  
Default: ``` openvpnClient_protocol: tcp ```  
************


#### openvpnClient_server_host 
Desc:  Host used for connection (with OVPN server on-board) 
Ways: ip/dnsName  
Default: ``` openvpnClient_server_host: vpn.rxlab.ru ```  
************

#### openvpnClient_use_tls 
Desc:  Host used for connection (with OVPN server on-board)   
Ways: yes/no  
Default: ``` openvpnClient_use_tls: yes ```  
************


### Example Playbook 
```
---
- hosts: U16
  remote_user: user
  roles:
     - { role: openvpnClient, tags: ["openvpnClient", "install", "config"] }
```


### License

MIT 

### Author Information

This role was created by [ep4sh](https://github.com/ep4sh/).